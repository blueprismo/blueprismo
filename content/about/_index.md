---
title: ABOUT
description: Hey, I'm Enin Kaduk
images: ["/images/sample.jpg"]

---


This is my about page. :wave:

Want to know more about me, huh? :smile:  

![Me, haha i'm ugly](../images/me.JPG)

This is me back when I got hair. 

It all begins in 5th december 1998, I was born in France with a lovely twin, he's the best present life has ever given to me.
I live in Spain, and work as a Sys@dmin in a GIS company for the moment.

While I work, I also study at the Univeristy (Computer Science)

During my free time, I do calisthenics, indoor rock-climbing and apnea.


## Why did I choose my nickname as [blue]prismo?
Blue is one of my favorite colous, and prismo is one of my favourite fictional characters. He's a wish master who is the manifestation of a very old man's dream.

![Enin's image](../images/prismo.png)
>"You see Jake, there's rules to this stuff.
> Wishing an event to be changes elements before and after it. Memories will be destroyed, babies will not be born, potential worlds could be evaporated by your wish. "

:earth_africa:
