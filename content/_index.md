---
title: Welcome!
description: Blueprismo's page

---

Hey,

I'm BluePrismo, some folk doomed to exist.

![alt text](images/rain2.gif)

I love learning new things, chess and prolonging my breath hold. Also aproaching life like a good estoic: **Things are not goot nor bad, they are just things.**

I do enjoy playing chess, you can find me on [lichess](https://lichess.org/@/eninkaduk)

If you want to keep falling on the rabbit hole -> [Get to know me better](./about "Get to know me better") 

---
